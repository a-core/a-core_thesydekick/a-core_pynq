`timescale 1ns / 1ps
`default_nettype none

// split 3-bit port to three wires in block design
module port_splitter_2(
        input  wire [1:0] i_port,
        output wire o_b0,
        output wire o_b1
);
    assign o_b0 = i_port[0];
    assign o_b1 = i_port[1];
endmodule
