`timescale 1ns / 1ps
`default_nettype none

// split 4-bit port to four wires in block design
module port_splitter_4(
        input  wire [3:0] i_port,
        output wire o_b0,
        output wire o_b1,
        output wire o_b2,
        output wire o_b3    
    );
    assign o_b0 = i_port[0];
    assign o_b1 = i_port[1];
    assign o_b2 = i_port[2];
    assign o_b3 = i_port[3];
endmodule
